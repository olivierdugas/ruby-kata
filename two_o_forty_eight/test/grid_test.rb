require 'test_helper'

class TwoOFortyEightTest < Minitest::Test
  def setup
    @combiner = stub()
    @grid = ::TwoOFortyEight::Grid.new @combiner

    @expected = Hash.new(0)
    @received = Hash.new(0)
    @overridenGrid = [[2,4,8,16],[0,2,4,8],[0,0,2,4],[0,0,0,2]]
    @grid.override(@overridenGrid)

    @combiner.stubs(:squash).with(kind_of(Array)).returns([0,0,0,0])
    @combiner.stubs(:reverseSquash).with(kind_of(Array)).returns([0,0,0,0])
  end

  def test_newGrid_shouldContainOnly_oneNumber
    @grid = ::TwoOFortyEight::Grid.new @combiner
    @expected = {0 => 15, 2 => 1}
    
    getGrid
    
    assert_equal @expected, @received
  end

  def test_canOverrideCurrentGrid
    @expected = {0 => 6, 2 => 4, 4 => 3, 8 => 2, 16 => 1}

    getGrid

    assert_equal @expected, @received
  end

  def test_moveLeft_shouldCallCombiner_onEachRow_WhiteBoxNoMocha
    # Not found to be a very useful way of testing:
    # very rigid, no order check available, mocks are stricts
    @combiner = Minitest::Mock.new
    @grid = ::TwoOFortyEight::Grid.new @combiner
    @grid.override(@overridenGrid)
    4.times do @combiner.expect(:squash, nil, [Array] ) end

    @grid.moveLeft()

    @combiner.verify
  end

  def test_moveLeft_shouldCallCombiner_onEachRow
    @combiner.stubs(:squash).with(equals([2,4,8,16])).returns([1,2,3,4])
    @combiner.stubs(:squash).with(equals([0,2,4,8])).returns([0,1,2,3])
    @combiner.stubs(:squash).with(equals([0,0,2,4])).returns([0,0,1,2])
    @combiner.stubs(:squash).with(equals([0,0,0,2])).returns([0,0,0,1])
    @expected = {0 => 6, 1 => 4, 2 => 3, 3 => 2, 4 => 1}

    @grid.moveLeft()

    getGrid
    assert_equal @expected, @received
  end

  def test_moveRight_shouldCallReverseCombiner_onEachRow
    @combiner.stubs(:reverseSquash).with(equals([2,4,8,16])).returns([1,2,3,4])
    @combiner.stubs(:reverseSquash).with(equals([0,2,4,8])).returns([0,1,2,3])
    @combiner.stubs(:reverseSquash).with(equals([0,0,2,4])).returns([0,0,1,2])
    @combiner.stubs(:reverseSquash).with(equals([0,0,0,2])).returns([0,0,0,1])
    @expected = {0 => 6, 1 => 4, 2 => 3, 3 => 2, 4 => 1}

    @grid.moveRight()

    getGrid
    assert_equal @expected, @received
  end

  def test_moveUp_shouldCallCombiner_onEachColumn
    @combiner.stubs(:squash).with(equals([2,0,0,0])).returns([1,2,3,4])
    @combiner.stubs(:squash).with(equals([4,2,0,0])).returns([0,1,2,3])
    @combiner.stubs(:squash).with(equals([8,4,2,0])).returns([0,0,1,2])
    @combiner.stubs(:squash).with(equals([16,8,4,2])).returns([0,0,0,1])
    @expected = {0 => 6, 1 => 4, 2 => 3, 3 => 2, 4 => 1}

    @grid.moveUp()

    getGrid
    assert_equal @expected, @received
  end

  def test_moveDown_shouldCallCombiner_onEachColumn
    @combiner.stubs(:reverseSquash).with(equals([2,0,0,0])).returns([1,2,3,4])
    @combiner.stubs(:reverseSquash).with(equals([4,2,0,0])).returns([0,1,2,3])
    @combiner.stubs(:reverseSquash).with(equals([8,4,2,0])).returns([0,0,1,2])
    @combiner.stubs(:reverseSquash).with(equals([16,8,4,2])).returns([0,0,0,1])
    @expected = {0 => 6, 1 => 4, 2 => 3, 3 => 2, 4 => 1}

    @grid.moveDown()

    getGrid
    assert_equal @expected, @received
  end

private
  def getGrid
    resultGrid = @grid.getGrid
    resultGrid.each { |row| row.each { |value| @received[value] += 1 } }
  end
end
