require 'test_helper'

class CombinerTest < Minitest::Test
  def setup
    @combiner = ::TwoOFortyEight::Combiner.new
  end

  def test_squashEmptyArray_shouldReturnEmptyArray
    empty = [0,0,0,0]
    result = @combiner.squash(empty)
    assert_equal(empty, result)
  end

  def test_squashOneNumber_alreadySquashed_shouldReturnSame
    alreadySquashed = [2,0,0,0]
    result = @combiner.squash(alreadySquashed)
    assert_equal(alreadySquashed, result)
  end

  def test_squashOneNumber_unSquashedLeft_shouldSquashNumberToStartIndex
    unSquashed = [0,2,0,0]
    squashed = [2,0,0,0]
    
    result = @combiner.squash(unSquashed)

    assert_equal(squashed, result)
  end

  def test_squashOneNumber_unSquashedMid_shouldSquashNumberToStartIndex
    unSquashed = [0,0,2,0]
    squashed = [2,0,0,0]
    
    result = @combiner.squash(unSquashed)

    assert_equal(squashed, result)
  end

  def test_squashOneNumber_unSquashedRight_shouldSquashNumberToStartIndex
    unSquashed = [0,0,0,2]
    squashed = [2,0,0,0]
    
    result = @combiner.squash(unSquashed)

    assert_equal(squashed, result)
  end

  def test_squashMany_differentNumbers_shouldShiftThemUntilSideBySide
    unSquashed = [0,2,0,4]
    squashed = [2,4,0,0]

    result = @combiner.squash(unSquashed)

    assert_equal(squashed, result)
  end

  def test_squashTwoIdenticalNumbers_shouldAddThemTogether
    unSquashed = [0,2,2,0]
    squashed = [4,0,0,0]

    result = @combiner.squash(unSquashed)

    assert_equal(squashed, result)
  end

  def test_squashTwoIdenticalNumbers_separatedByZero_shouldAddThemTogether
    unSquashed = [0,2,0,2]
    squashed = [4,0,0,0]

    result = @combiner.squash(unSquashed)

    assert_equal(squashed, result)
  end

  def test_squashTwoIdenticalNumbers_separatedByManyZeros_shouldAddThemTogether
    unSquashed = [2,0,0,2]
    squashed = [4,0,0,0]

    result = @combiner.squash(unSquashed)

    assert_equal(squashed, result)
  end

  def test_squashThreeIdenticalNumbers_shouldSquashFromLowestIndex
    unSquashed = [2,0,2,2,0,2,2]
    squashed = [4,4,2,0,0,0,0]

    result = @combiner.squash(unSquashed)

    assert_equal(squashed, result)
  end

  def test_reverseSquash_shouldSquashFromHighestIndex
    unSquashed = [2,0,2,2,0,2,2]
    squashed = [0,0,0,0,2,4,4]

    result = @combiner.reverseSquash(unSquashed)

    assert_equal(squashed, result)
  end
end
