module TwoOFortyEight
  class Grid
    def initialize(combiner)
      @combiner = combiner
      @grid = [[0,0,0,0],[0,0,0,0],[0,0,2,0],[0,0,0,0]]
    end

    def getGrid
      return @grid
    end

    def moveLeft
      for i in 0..3
        @grid[i] = @combiner.squash(@grid[i])
      end
    end

    def moveRight
      for i in 0..3
        @grid[i] = @combiner.reverseSquash(@grid[i])
      end
    end

    def moveUp
      for i in 0..3
        column = [0,0,0,0]
        for row in 0..3
          column[row] = @grid[row][i]
        end
        
        column = @combiner.squash(column)

        for row in 0..3
          @grid[row][i] = column[row]
        end
      end
    end

    def moveDown
      for i in 0..3
        column = [0,0,0,0]
        for row in 0..3
          column[row] = @grid[row][i]
        end
        
        column = @combiner.reverseSquash(column)

        for row in 0..3
          @grid[row][i] = column[row]
        end
      end
    end
    
    def override(grid)
      @grid = grid
    end
  end
end
