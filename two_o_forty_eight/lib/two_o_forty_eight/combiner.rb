module TwoOFortyEight
  class Combiner
    def squash(array)
      @array = array
      i = 0
      lastIndex = @array.length - 1
      
      while i < lastIndex && !restOfArrayIsZero?(i)
        #puts "array at #{i}: #{@array[i]} vs #{@array[i+1]}"
        
        if @array[i] == 0
          shiftFromIndex(i)

        else
          shiftNullNeighbors(i)
          combineIfIdenticalNeighbor(i)
          i += 1
        end
      end
      return @array
    end

    def reverseSquash(array)
      array.reverse!
      return squash(array).reverse
    end

  private
    def restOfArrayIsZero?(fromIndex)
      allZeros = true
      for i in fromIndex...@array.length
        allZeros = allZeros && (@array[i] == 0)
      end
      return allZeros
    end

    def shiftFromIndex(fromIndex)
      for i in fromIndex...(@array.length - 1)
        @array[i] = @array[i+1]
        @array[i+1] = 0
      end
    end

    def combineIfIdenticalNeighbor(index)
      if @array[index] == @array[index+1]
        @array[index] *= 2
        @array[index+1] = 0
      end
    end

    def shiftNullNeighbors(index)
      while @array[index+1] == 0 && !restOfArrayIsZero?(index+1)
        shiftFromIndex(index+1)
      end
    end
  
  end
end
