module Kata
  class StringCalculator
    def initialize
      @sum = 0
      @delimiters = ","
    end

    def add(numbers)
      initialize
      numbers = parseDelimiterOverride(numbers)
      computeSum(numbers)
      return @sum
    end
    
  private

    def computeSum(numbers)
      for number in numbers.split(/#{@delimiters}|\n/)
        @sum += number.to_i
      end
    end


    def parseDelimiterOverride(numbers)
      if numbers.start_with?("//")
        @delimiters = numbers.split("\n")[0][2..-1]
        return removeDelimiterOverrideLine(numbers)
      else
        return numbers
      end
    end


    def removeDelimiterOverrideLine(string)
      string[string.index("\n")+1..-1]
    end
  end
end
