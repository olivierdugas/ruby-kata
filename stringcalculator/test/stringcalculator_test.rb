require 'test_helper'

class StringcalculatorTest < Minitest::Test
  EXPECTED_SUM = 42

  def test_that_it_has_a_version_number
    refute_nil ::Kata::VERSION
  end

  def setup
    @calculator = ::Kata::StringCalculator.new
  end

  def test_addEmptyString_shouldReturn0
    assert_equal 0, @calculator.add("")
  end
  
  def test_addOneNumber_shouldReturnThisNumber
    expect42AfterAdding("42")
  end
  
  def test_addTwoNumbers_shouldReturnSum
    expect42AfterAdding("40,2")
  end
  
  def test_addAnyAmountOfNumbers_shouldReturnSum
    expect42AfterAdding("20,20,2")
  end
  
  def test_newLineInsteadOfComma_shouldReturnSum
    expect42AfterAdding("20\n20,2")
  end
  
  def test_separatorsAtTheEndOfNumbers_shouldBeIgnored
    expect42AfterAdding("40,2\n,")
  end
  
  def test_separatorsAtTheStartOfNumbers_shouldBeIgnored
    expect42AfterAdding(",\n,40,2")
  end
  
  def test_changingDelimiter_shouldUseNewDelimiterToComputeSum
    expect42AfterAdding("//;\n40;2")
  end

  def test_changingDelimiter_shouldStillUseNewlineAsDelimiter
    expect42AfterAdding("//;\n20;20\n2")
  end

private
  def expect42AfterAdding(input)
    assert_equal EXPECTED_SUM, @calculator.add(input)
  end
end
